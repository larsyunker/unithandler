import unittest
import random
import math
# import numpy as np
from copy import copy
from unithandler.base import Unit, UnitFloat, UnitInt, SI_PREFIXES, scale_value_by_prefix, adjust_unit


class TestSCBP(unittest.TestCase):
    """Tests the scale_value_by_prefix method"""
    actual = 2.5

    def test(self):
        self.assertEqual(
            self.actual * 10 ** 3,
            scale_value_by_prefix(self.actual, 'm')
        )
        self.assertEqual(
            self.actual * 10 ** -9,
            scale_value_by_prefix(self.actual, 'G')
        )
        self.assertEqual(
            self.actual * 10 ** (12 - 6),  # pico to micro
            scale_value_by_prefix(self.actual, 'p', 'u')
        )


class TestUnit(unittest.TestCase):
    """Tests the behaviour of the Unit class"""
    def test_class(self):
        true = {
            'kg': -5,
            'm': 7,
            's': -123,
            'A': 1,
            'L': 1,
        }
        uni = Unit('kg-5 m7/s123·A*L')
        # equality
        self.assertEqual(uni.units, true)
        self.assertEqual(uni, true)
        self.assertEqual(uni.inverse(), {unit: -true[unit] for unit in true})
        self.assertNotEqual(uni.units, 'm s')
        self.assertNotEqual(uni, 'm s')

        # methods
        self.assertTrue(uni.unit_equality(true))
        self.assertTrue(uni.contains_unit('m/s'))
        self.assertTrue(uni.contains_inverse_unit('kg/m'))

        # multiplication
        self.assertEqual(
            uni * 'm/s',
            adjust_unit(
                true,
                {'m': 1, 's': -1},
            )
        )
        rev = 'm/s' * uni
        self.assertEqual(
            rev.units,
            adjust_unit(
                {'m': 1, 's': -1},
                uni.units,
            )
        )
        adj = copy(uni)
        adj *= 'kg/s'
        self.assertEqual(
            adj.units,
            adjust_unit(uni.units, {'kg': 1, 's': -1})
        )

        # division
        self.assertEqual(
            uni / 'm/s',
            adjust_unit(true, {'m': -1, 's': 1})
        )
        rev = 'm/s' / uni
        self.assertEqual(
            rev.units,
            adjust_unit(
                {'m': 1, 's': -1},
                uni.inverse(),
            )
        )
        adj = copy(uni)
        adj /= 'kg/s'
        self.assertEqual(
            adj.units,
            adjust_unit(uni.units, {'kg': -1, 's': 1})
        )

        # power
        self.assertEqual(
            uni ** 2,
            {unit: uni.units[unit] * 2 for unit in uni.units}
        )
        adj = copy(uni)
        adj **= 3
        self.assertEqual(
            adj,
            {unit: uni.units[unit] * 3 for unit in uni.units}
        )

        # non-unicode representation
        non_uni_repr = uni.create_string_representation(use_unicode=False)
        self.assertEqual(  # check that returned representation is unicode
            len(non_uni_repr),
            len(non_uni_repr.encode())
        )

    def test_input(self):
        """tests input type acceptance and error raising of Unit class"""
        true_units = {
            'mol': -1,
            'm': 7,
            's': -123,
            'A': 1,
            'L': 3,
        }
        self.assertEqual(
            Unit(f'{" ".join([f"{key}{power}" for key, power in true_units.items()])}').units,
            true_units,
        )
        self.assertEqual(
            Unit(true_units).units,
            true_units,
        )
        self.assertEqual(
            Unit(
                Unit(true_units)
            ).units,
            true_units
        )
        for val in [1, 1., True]:
            with self.assertRaises(TypeError):
                Unit(val)


class TestUnitFloat(unittest.TestCase):
    """A test class for UnitFloat"""
    def setUp(self):
        self.actual = 1.234
        self.mod = 2.234
        self.stored_prefix = 'm'
        self.val = UnitFloat(  # 1.5 mL stored in the milli prefix
            self.actual,
            'L',
            self.stored_prefix,
            self.stored_prefix
        )

    def test_comparisons(self):
        """Tests comparisons of UnitFloat"""

        # in/equality checks
        self.assertEqual(self.val.real, self.actual)  # test equality of stored float
        self.assertEqual(self.val, self.actual)  # check builtin equal
        self.assertNotEqual(self.val, self.actual + self.mod)

        # greater than checks
        self.assertGreater(self.val, self.actual - self.mod)  # greater than
        self.assertGreaterEqual(self.val, self.actual - self.mod)
        self.assertGreaterEqual(self.val, self.actual)

        # less than checks
        self.assertLess(self.val, self.actual + self.mod)
        self.assertLessEqual(self.val, self.actual + self.mod)
        self.assertLessEqual(self.val, self.actual)

    def test_numerical(self):
        """Tests numerical operations on UnitFloat"""
        # addition
        self.assertEqual(self.actual + self.mod, self.val + self.mod)  # add
        self.assertEqual(self.mod + self.actual, self.mod + self.val)  # radd
        val2 = copy(self.val)
        val2 += self.mod  # iadd
        self.assertEqual(self.actual + self.mod, val2)

        # subtraction
        self.assertEqual(self.actual - self.mod, self.val - self.mod)  # sub
        self.assertEqual(self.mod - self.actual, self.mod - self.val)  # rsub
        val2 = copy(self.val)
        val2 -= self.mod  # isub
        self.assertEqual(self.actual - self.mod, val2)  # isub

        # multiplication
        self.assertEqual(self.actual * self.mod, self.val * self.mod)  # rmul
        self.assertEqual(self.mod * self.actual, self.mod * self.val)  # mul
        val2 = copy(self.val)
        val2 *= self.mod  # imul
        self.assertEqual(self.actual * self.mod, val2)

        # division
        self.assertEqual(self.actual / self.mod, self.val / self.mod)  # div
        # rdiv is a special case where the numerator is assumed to have no prefix
        self.assertEqual(self.mod / 10 ** SI_PREFIXES[self.val.prefix] / self.actual, self.mod / self.val)  # rdiv
        val2 = copy(self.val)
        val2 /= self.mod  # idiv
        self.assertEqual(self.actual / self.mod, val2)

        # divmod
        self.assertEqual(self.actual // self.mod, self.val // self.mod)
        self.assertEqual(self.actual % self.mod, self.val % self.mod)

        # power
        self.assertEqual(self.actual ** self.mod, self.val ** self.mod)  # pow
        val2 = copy(self.val)
        val2 **= self.mod  # ipow
        self.assertEqual(self.actual ** self.mod, val2)

    def test_uf_division(self):
        """
        Special case for testing division of a UnitFloat by another
        The resulting instance should have no prefix for further mathematical operations.
        """
        vol = UnitFloat(0.05, 'mL')
        flow = UnitFloat(1., 'mL/min')
        scalar = UnitFloat(60, 's/min')

        # check div
        result = vol / flow
        self.assertEqual(result.prefix, '', 'check that prefix was correctly removed')
        final_result = result * scalar
        self.assertEqual(final_result, 3, 'check that further operations yield the correct magnitude of result')
        self.assertEqual(float(final_result), 3)

        # check idiv
        vol /= flow
        self.assertEqual(vol.prefix, '', 'check that prefix was correctly removed')
        self.assertEqual(vol, result, 'check that result is the same as doing it separately')
        self.assertEqual(vol.units, result.units)
        vol *= scalar
        self.assertEqual(vol, final_result, 'check calculated value')
        self.assertEqual(float(vol), 3, 'check actual value')

    def test_operators(self):
        """tests basic python operations"""
        self.assertEqual(self.actual, float(self.val))
        self.assertEqual(int(self.actual), int(self.val))
        copyval = copy(self.val)
        copyval.prefix = 'n'
        multiplied = self.val * copyval
        self.assertEqual(self.val.prefix, multiplied.prefix)

    def test_methods(self):
        """Tests the methods of UnitFloat"""
        self.assertEqual(  # specific prefix
            self.actual * 10 ** -(SI_PREFIXES['k'] - SI_PREFIXES[self.val.prefix]),
            self.val.specific_prefix('k')
        )
        val2 = copy(self.val)
        val2.prefix = 'G'
        self.assertEqual(
            self.actual * 10 ** (-3 - 9),
            val2
        )
        for i in range(1000):  # test a bunch of numbers
            val3 = round(random.random(), 6)
            val4 = UnitFloat(val3, 'L')
            self.assertAlmostEqual(
                round(val3, 10),
                round(val4, 10)
            )

            # check prefix scaling
            val4.prefix = 'm'
            self.assertAlmostEqual(
                val3 * 1000.,
                val4,
            )
            val4.prefix = 'k'
            self.assertAlmostEqual(
                val3 / 1000.,
                val4
            )

    def test_string_input(self):
        """tests string input of values"""
        value = round(random.uniform(1, 999), 3)
        from_string = UnitFloat(f'{value} g/mol')
        from_value = UnitFloat(value, 'g/mol')
        self.assertEqual(
            from_string.units,
            from_value.units
        )
        self.assertEqual(
            from_string.real,
            from_value.real,
        )
        with self.assertRaises(ValueError):
            UnitFloat(f'{random.random()}a g/mol')
        self.assertEqual(
            from_string,
            UnitFloat(str(from_string))
        )

    def test_prefix_extraction(self):
        """tests automatic prefix determination"""
        self.assertEqual(  # general test
            UnitFloat(1., 'ug').units,
            {'g': 1}
        )
        self.assertEqual(
            UnitFloat(900., 'MW').units,
            {'W': 1}
        )
        self.assertEqual(  # ignore conflict units
            UnitFloat(1., 'm').units,
            {'m': 1}
        )
        self.assertEqual(
            UnitFloat(1., 'min').units,
            {'min': 1}
        )
        self.assertEqual(  # ensure specified stored prefix is honoured
            UnitFloat(1., 'ug', stored_prefix='m').prefix,
            'm'
        )
        self.assertEqual(
            UnitFloat(1., 'mol/L').units,
            {'mol': 1, 'L': -1}
        )
        self.assertEqual(
            UnitFloat(1., 'newton').units,
            {'newton': 1}
        )

    # def test_numpy(self):
    #     """Tests a variety of numpy operations"""
    #     mm = UnitFloat(1, 'm', 'm', 'm')
    #     x_true = 187.5
    #     y_true = 156.
    #     x = x_true * mm
    #     y = y_true * mm
    #     self.assertEqual(np.sin(x_true), np.sin(x))
    #     self.assertEqual(np.real(x_true), np.real(x))
    #     self.assertEqual(np.imag(x_true), np.imag(x))
    #     self.assertEqual(np.sqrt(x_true), np.sqrt(x))
    #     self.assertEqual(np.square(x_true), np.square(x))
    #     self.assertEqual(
    #         np.arctan2(x_true, y_true),
    #         np.arctan2(x, y)
    #     )
    #     self.assertEqual(
    #         np.arange(y_true, x_true, 50),
    #         np.arange(y, x, 50)
    #     )
    #     self.assertEqual(np.round(x, 1), np.round(x_true, 1))

    def test_scale_lock(self):
        """tests user disabling of auto scaling"""
        scaled = UnitFloat(0.001, 'm')
        unscaled = UnitFloat(0.001, 'm', scale_representation=False)
        self.assertEqual(scaled, unscaled)
        self.assertEqual(
            unscaled.unit,
            'm',
        )

    def test_passed_unitvalues(self):
        """tests cases where a UnitFloat is passed to another UnitFloat init"""
        inherited = UnitFloat(self.val)
        self.assertEqual(
            self.val,
            inherited
        )
        self.assertEqual(
            self.val.units,
            inherited.units
        )
        self.assertEqual(
            self.val.prefix,
            inherited.prefix,
        )
        self.assertEqual(
            self.val.scale_representation,
            inherited.scale_representation
        )

    def test_rounding(self):
        """tests round method of UnitFloat"""
        number = 1.234567890123456789
        big_val = UnitFloat(number, 'YL', stored_prefix='')
        little_val = UnitFloat(number, 'uL', stored_prefix='')

        for prefix, power in SI_PREFIXES.items():
            if prefix == '':
                continue
            if prefix == 'da':  # todo dig into why this fails
                continue
            if power > 0:
                val = big_val
            else:
                val = little_val
            rounded_by_power = copy(val)
            rounded_by_power.round(-power)
            self.assertAlmostEqual(
                rounded_by_power.real,
                round(val.real, -power),
                msg=f'rounded by power {prefix, power}'
            )
            rounded_by_prefix = copy(val)
            rounded_by_prefix.round(prefix=prefix)
            self.assertAlmostEqual(
                rounded_by_prefix.real,
                round(val.real, -power),
                msg=f'rounded by prefix {prefix, power}'
            )

    def test_math(self):
        """tests an assortment of math operations"""
        mm = UnitFloat(1, 'm', 'm', 'm')
        x_true = 187.5
        y_true = 156.
        x = x_true * mm
        y = y_true * mm
        self.assertEqual(math.sin(x_true), math.sin(x))
        self.assertEqual(math.sqrt(x_true), math.sqrt(x))
        self.assertEqual(
            math.atan2(x_true, y_true),
            math.atan2(x, y)
        )


class TestUnitInt(unittest.TestCase):
    """Test methods for UnitInt"""
    def setUp(self):
        self.actual = random.choice([i for i in range(10000)])
        self.mod_float = random.random()
        self.mod_int = random.choice([i for i in range(10000)])
        self.val = UnitInt(self.actual, 'L')

    def test_comparisons(self):
        """Tests comparison methods"""
        # in/equality checks
        self.assertEqual(self.val.real, self.actual)  # test equality of stored int
        self.assertEqual(self.val, self.actual)  # check builtin equal
        self.assertNotEqual(self.val, self.actual + self.mod_float)

        # greater than checks
        self.assertGreater(self.val, self.actual - self.mod_float)  # greater than
        self.assertGreaterEqual(self.val, self.actual - self.mod_float)
        self.assertGreaterEqual(self.val, self.actual)

        # less than checks
        self.assertLess(self.val, self.actual + self.mod_float)
        self.assertLessEqual(self.val, self.actual + self.mod_float)
        self.assertLessEqual(self.val, self.actual)

    def test_numerical(self):
        """Tests numerical operations on UnitFloat"""
        # addition
        self.assertEqual(self.actual + self.mod_int, self.val + self.mod_int)  # add integer
        self.assertEqual(self.actual + self.mod_float, self.val + self.mod_float)  # add float (converts to UnitFloat)
        self.assertEqual(self.mod_int + self.actual, self.mod_int + self.val)  # radd
        val2 = copy(self.val)
        val2 += self.mod_int  # iadd
        self.assertEqual(self.actual + self.mod_int, val2)
        val2 = copy(self.val)
        val2 += self.mod_float  # iadd to float
        self.assertEqual(self.actual + self.mod_float, val2)

        # subtraction
        self.assertEqual(self.actual - self.mod_int, self.val - self.mod_int)  # sub
        self.assertEqual(self.actual - self.mod_float, self.val - self.mod_float)  # sub
        self.assertEqual(self.mod_int - self.actual, self.mod_int - self.val)  # rsub
        val2 = copy(self.val)
        val2 -= self.mod_int  # isub
        self.assertEqual(self.actual - self.mod_int, val2)  # isub
        val2 = copy(self.val)
        val2 -= self.mod_float  # isub
        self.assertEqual(self.actual - self.mod_float, val2)  # isub

        # multiplication
        self.assertEqual(self.actual * self.mod_int, self.val * self.mod_int)  # mul
        self.assertEqual(self.mod_int * self.actual, self.mod_int * self.val)  # rmul
        val2 = copy(self.val)
        val2 *= self.mod_int  # imul
        self.assertEqual(self.actual * self.mod_int, val2)

        # division
        self.assertEqual(self.actual / self.mod_int, self.val / self.mod_int)  # div
        self.assertEqual(self.mod_int / self.actual, self.mod_int / self.val)  # rdiv
        val2 = copy(self.val)
        val2 /= self.mod_int  # idiv
        self.assertEqual(self.actual / self.mod_int, val2)

        # divmod
        self.assertEqual(self.actual // self.mod_int, self.val // self.mod_int)
        self.assertEqual(self.actual % self.mod_int, self.val % self.mod_int)

        # power
        self.assertEqual(self.actual ** self.mod_int, self.val ** self.mod_int)  # pow
        val2 = copy(self.val)
        val2 **= self.mod_int  # ipow
        self.assertEqual(self.actual ** self.mod_int, val2)

    def test_string_input(self):
        """tests string input of values"""
        value = int(random.randint(1, 1000))
        from_string = UnitInt(f'{value} g/mol')
        from_value = UnitInt(value, 'g/mol')
        self.assertEqual(
            from_string.units,
            from_value.units
        )
        self.assertEqual(
            from_string.real,
            from_value.real,
        )
        with self.assertRaises(ValueError):
            UnitInt(f'{value}a g/mol')
        self.assertEqual(
            from_string,
            UnitFloat(str(from_string))
        )

    # def test_numpy(self):
    #     """Tests a variety of numpy operations"""
    #     mm = UnitInt(1, 'mm')
    #     x_true = 187
    #     y_true = 156
    #     x = x_true * mm
    #     y = y_true * mm
    #     self.assertEqual(np.sin(x_true), np.sin(x))
    #     self.assertEqual(np.real(x_true), np.real(x))
    #     self.assertEqual(np.imag(x_true), np.imag(x))
    #     self.assertEqual(np.sqrt(x_true), np.sqrt(x))
    #     self.assertEqual(np.square(x_true), np.square(x))
    #     self.assertEqual(
    #         np.arctan2(x_true, y_true),
    #         np.arctan2(x, y)
    #     )
    #     self.assertEqual(
    #         np.arange(y_true, x_true, 50),
    #         np.arange(y, x, 50)
    #     )
    #     self.assertEqual(np.round(x, 1), np.round(x_true, 1))

    def test_math(self):
        """tests an assortment of math operations"""
        mm = UnitInt(1, 'mm')
        x_true = 187
        y_true = 156
        x = x_true * mm
        y = y_true * mm
        self.assertEqual(math.sin(x_true), math.sin(x))
        self.assertEqual(math.sqrt(x_true), math.sqrt(x))
        self.assertEqual(
            math.atan2(x_true, y_true),
            math.atan2(x, y)
        )

if __name__ == '__main__':
    unittest.main(verbosity=2)
